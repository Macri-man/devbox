#!/usr/bin/env bash
set -e
mkdir .ssh 2>/dev/null || true
echo "gitlab.com,52.21.36.51 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=" > ~/.ssh/known_hosts
sudo sed --in-place /etc/apt/sources.list -e 's/archive.ubuntu.com/mirror.clarkson.edu/g'
sudo apt-get update && apt-get upgrade && apt-get dist-upgrade
sudo apt-get install -y python python-pip gcc-multilib qemu-system libxml2 build-essential git clang
sudo pip install tempita

sed -i '1i export RUST_TARGET_PATH=/vagrant/rust-sel4/targets' ~/.bashrc
source ~/.bashrc

pushd /vagrant >/dev/null
bash get_new_repos.sh
popd >/dev/null
if hash rustc 2>/dev/null; then
	true
else
	curl -sSOf https://static.rust-lang.org/rustup.sh
	sudo bash rustup.sh --yes --channel=nightly --date=2015-09-13 --prefix=/usr
fi

RUSTC_REVISION=$(rustc -vV | awk '/commit-hash/ { print $2; }')

mkdir -p /usr/lib/rustlib/i686-unknown-sel4/lib
if [ ! -e rust ]; then
	git clone https://github.com/rust-lang/rust
fi
pushd rust >/dev/null
git fetch
git checkout $RUSTC_REVISION
git submodule update --init --recursive --depth 1
popd  >/dev/null

pushd /vagrant/rust-sel4 >/dev/null
git submodule init
git submodule update --depth 1
popd >/dev/null

if [ "$(cat /usr/lib/rustlib/i686-unknown-sel4/lib/libcompiler_rt.a.stamp 2>/dev/null)" != "$RUSTC_REVISION" ]; then
    pushd rust >/dev/null
    cd src/compiler-rt
    [ -d build ] || mkdir build
    make -j3 ProjSrcRoot=$(pwd) ProjObjRoot=$(pwd)/build TargetTriple=i686-unknown-linux-gnu triple-builtins
    sudo cp build/triple/builtins/libcompiler_rt.a /usr/lib/rustlib/i686-unknown-sel4/lib
    echo $RUSTC_REVISION | sudo tee /usr/lib/rustlib/i686-unknown-sel4/libcompiler_rt.a.stamp > /dev/null
    popd >/dev/null
fi
if [ "$(cat /usr/lib/rustlib/i686-unknown-sel4/lib/libcore.rlib.stamp 2>/dev/null)" != "$RUSTC_REVISION" ]; then
    pushd rust/src/libcore >/dev/null
    rustc -O --target=i686-unknown-sel4 lib.rs
    sudo cp libcore.rlib /usr/lib/rustlib/i686-unknown-sel4/lib
    echo $RUSTC_REVISION | sudo tee /usr/lib/rustlib/i686-unknown-sel4/lib/libcore.rlib.stamp > /dev/null
    popd >/dev/null
fi

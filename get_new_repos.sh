#!/bin/bash
set -e
if ssh -T git@gitlab.com; then
    proto=ssh
    extra=git@
else
    proto=https
    extra=
fi

new_repo() {
    if [ ! -d $1 ]; then
        git clone --recursive $2 $proto://${extra}gitlab.com/robigalia/$1.git
        if grep -v -q "/$1" .gitignore; then
            echo "/$1" >> .gitignore;
        fi
    else
        echo Not cloning already-existing repo $1
    fi
}

new_repo rust-sel4
new_repo robigo
